﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy Video.xaml
    /// </summary>
    public partial class Video : Page
    {
        private const string UriString = "C:\\Users\\Olek\\zespolowy-projekt-badawczy\\ProjektBadawczy\\Res\\czarnedziury.mp4";

        public Video()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += new EventHandler(timer_Tick);

            //string filename = (string)((DataObject)e.Data).GetFileDropList()[0];
            mediaElement1.Source = new Uri(UriString);
            mediaElement1.LoadedBehavior = MediaState.Manual;
            mediaElement1.UnloadedBehavior = MediaState.Manual;
            mediaElement1.Volume = (double)slider_vol.Value;
        }

        DispatcherTimer timer;
       
        void timer_Tick(object sender, EventArgs e)
        {
            slider_seek.Value = mediaElement1.Position.TotalSeconds;
            
            if(mediaElement1.Position.TotalSeconds >= 120) {
                if (mediaElement1.Source != null)
                        {
                            mediaElement1.Stop();
                        MessageBox.Show("Wypełnij teraz quiz");
                        QuizVideo qvideo = new QuizVideo();
                        this.NavigationService.Navigate(qvideo);
                }             
                }
        }
            private void btPlay_Click(object sender, RoutedEventArgs e)
        {
            if (mediaElement1.Source != null)
            {
                mediaElement1.Play();
            }
           
        }
        //private void btPause_Click(object sender, RoutedEventArgs e)
        //{
        //    if (mediaElement1.Source != null)
        //    {
        //        mediaElement1.Pause();
        //    }
        //    else
        //    {
        //        MessageBox.Show("Brak wideo do odtworzenia");
        //    }
        //}
        //private void btStop_Click(object sender, RoutedEventArgs e)
        //{
        //    if (mediaElement1.Source != null)
        //    {
        //        mediaElement1.Stop();
        //    }
        //    else
        //    {
        //        MessageBox.Show("Brak wideo do zatrzymania");
        //    }
        //}
        private void slider_vol_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement1.Volume = (double)slider_vol.Value;
        }
        private void slider_seek_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement1.Position = TimeSpan.FromSeconds(slider_seek.Value);
        }
       
        private void mediaElement1_MediaOpened(object sender, RoutedEventArgs e)
        {
            TimeSpan ts = mediaElement1.NaturalDuration.TimeSpan;
            slider_seek.Maximum = ts.TotalSeconds;
            timer.Start();
        }



    }
}
