﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy QuizTekst1.xaml
    /// </summary>
    public partial class QuizTekst1 : Page
    {
        public QuizTekst1()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Btn1.Foreground = Brushes.Blue;
            Btn1.Background = Brushes.Yellow;
            Btn2.Foreground = Brushes.Black;
            Btn2.Background = Brushes.White;
            Btn3.Foreground = Brushes.Black;
            Btn3.Background = Brushes.White;
            Btn4.Foreground = Brushes.Black;
            Btn4.Background = Brushes.White;
        }

        private void Btn2_Checked(object sender, RoutedEventArgs e)
        {
            Btn2.Foreground = Brushes.Blue;
            Btn2.Background = Brushes.Yellow;
            Btn1.Foreground = Brushes.Black;
            Btn1.Background = Brushes.White;
            Btn3.Foreground = Brushes.Black;
            Btn3.Background = Brushes.White;
            Btn4.Foreground = Brushes.Black;
            Btn4.Background = Brushes.White;
        }

        private void Btn3_Checked(object sender, RoutedEventArgs e)
        {
            Btn3.Foreground = Brushes.Blue;
            Btn3.Background = Brushes.Yellow;
            Btn2.Foreground = Brushes.Black;
            Btn2.Background = Brushes.White;
            Btn1.Foreground = Brushes.Black;
            Btn1.Background = Brushes.White;
            Btn4.Foreground = Brushes.Black;
            Btn4.Background = Brushes.White;
        }

        private void Btn4_Checked(object sender, RoutedEventArgs e)
        {
            Btn4.Foreground = Brushes.Blue;
            Btn4.Background = Brushes.Yellow;
            Btn2.Foreground = Brushes.Black;
            Btn2.Background = Brushes.White;
            Btn3.Foreground = Brushes.Black;
            Btn3.Background = Brushes.White;
            Btn1.Foreground = Brushes.Black;
            Btn1.Background = Brushes.White;
        }

        private void Btn5_Checked(object sender, RoutedEventArgs e)
        {
            Btn5.Foreground = Brushes.Blue;
            Btn5.Background = Brushes.Yellow;
            Btn6.Foreground = Brushes.Black;
            Btn6.Background = Brushes.White;
            Btn7.Foreground = Brushes.Black;
            Btn7.Background = Brushes.White;
            Btn8.Foreground = Brushes.Black;
            Btn8.Background = Brushes.White;
        }

        private void Btn6_Checked(object sender, RoutedEventArgs e)
        {
            Btn6.Foreground = Brushes.Blue;
            Btn6.Background = Brushes.Yellow;
            Btn5.Foreground = Brushes.Black;
            Btn5.Background = Brushes.White;
            Btn7.Foreground = Brushes.Black;
            Btn7.Background = Brushes.White;
            Btn8.Foreground = Brushes.Black;
            Btn8.Background = Brushes.White;
        }

        private void Btn7_Checked(object sender, RoutedEventArgs e)
        {
            Btn7.Foreground = Brushes.Blue;
            Btn7.Background = Brushes.Yellow;
            Btn6.Foreground = Brushes.Black;
            Btn6.Background = Brushes.White;
            Btn5.Foreground = Brushes.Black;
            Btn5.Background = Brushes.White;
            Btn8.Foreground = Brushes.Black;
            Btn8.Background = Brushes.White;
        }

        private void Btn8_Checked(object sender, RoutedEventArgs e)
        {
            Btn8.Foreground = Brushes.Blue;
            Btn8.Background = Brushes.Yellow;
            Btn6.Foreground = Brushes.Black;
            Btn6.Background = Brushes.White;
            Btn7.Foreground = Brushes.Black;
            Btn7.Background = Brushes.White;
            Btn5.Foreground = Brushes.Black;
            Btn5.Background = Brushes.White;
        }

        private void QuizTekstKoniec_Click(object sender, RoutedEventArgs e)
        {
            if (Btn2.IsChecked == true & Btn8.IsChecked == true)
            { MessageBox.Show("Odpowiedziałeś poprawnie, przejdź dalej"); }
            else if(Btn2.IsChecked == true || Btn8.IsChecked == true)
            {
            MessageBox.Show("Odpowiedziałeś poprawnie na jedno pytanie, przejdź dalej");
            } 
            else
            {
            MessageBox.Show("Odpowiedziałeś niepoprawnie na  pytania, przejdź dalej");
            }

            Btn1.IsEnabled = false;
            Btn2.IsEnabled = false;
            Btn3.IsEnabled = false;
            Btn4.IsEnabled = false;
            Btn5.IsEnabled = false;
            Btn6.IsEnabled = false;
            Btn7.IsEnabled = false;
            Btn8.IsEnabled = false;
            Video video = new Video();
            this.NavigationService.Navigate(video);
        }
    }
    
}
