﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy Zasady.xaml
    /// </summary>
    public partial class Zasady : Page
    {
        public Zasady()
        {
            InitializeComponent();

        }

        private void ZasadySubmit_Click(object sender, RoutedEventArgs e)
        {
            Tekst tekst = new Tekst();
            this.NavigationService.Navigate(tekst);
        }
    }
}
