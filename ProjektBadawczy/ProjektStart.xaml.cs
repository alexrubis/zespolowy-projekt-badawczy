﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy ProjektStart.xaml
    /// </summary>
    public partial class ProjektStart : Page
    {
        private Storyboard myStoryboard;
        public ProjektStart()
        {
            InitializeComponent();
            Grid myGrid = new Grid();
            TextBlock myTextBlock = new TextBlock();
            myTextBlock.Name = "title3";
            this.RegisterName(myTextBlock.Name, myTextBlock);
           

            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = 0.0;
            myDoubleAnimation.To = 1.0;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(3));
            myDoubleAnimation.AutoReverse = false;
            //myDoubleAnimation.RepeatBehavior = RepeatBehavior.Forever;

            myStoryboard = new Storyboard();
            myStoryboard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, myTextBlock.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Rectangle.OpacityProperty));

            // Use the Loaded event to start the Storyboard.
            myTextBlock.Loaded += new RoutedEventHandler(myRectangleLoaded);
            myGrid.Children.Add(myTextBlock);
        }
        private void myRectangleLoaded(object sender, RoutedEventArgs e)
        {
            myStoryboard.Begin(this);
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            Zasady zasady = new Zasady();
            this.NavigationService.Navigate(zasady);
        }
    }
}
