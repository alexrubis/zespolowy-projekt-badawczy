﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy Tekst.xaml
    /// </summary>
    public partial class Tekst : Page
    {
        
        public Tekst()
        {

            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += dispatcherTimer_Tick;
            timer.Start();

        }
        int time = 0;
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // Updating the Label which displays the current second
            time += 1;
            lblSeconds.Content = "Czas: "+time.ToString()+"s";

        }

        private void tekstKoniec_Click(object sender, RoutedEventArgs e)
        {
            QuizTekst1 qt = new QuizTekst1();
            this.NavigationService.Navigate(qt);
        }
    }
}
