﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektBadawczy
{
    /// <summary>
    /// Logika interakcji dla klasy ZadaniaMat.xaml
    /// </summary>
    public partial class ZadaniaMat : Page
    {
        public ZadaniaMat()
        {
            InitializeComponent();
        }

        private void Policz()
        {
            int punkty = 0;
            
            var labelsAndTextB = this.panel1.Children.OfType<Label>().Zip(this.panel1.Children.OfType<TextBox>(), (l, t) => new { labl = l, txtBox = t });
            foreach (var lt in labelsAndTextB)
            {
                //MessageBox.Show(lt.labl.Content.ToString()+"  " + lt.txtBox.Text.ToString());

                if (lt.labl.Content.ToString() != "" & lt.txtBox.Text != "")
                {
                    string lb = lt.labl.Content.ToString().Remove(0, 3);
                    double result = Convert.ToDouble(new DataTable().Compute(lb, null));
                    double userResult = Convert.ToDouble(new DataTable().Compute(lt.txtBox.Text, null));
                    if (result == userResult) { punkty++; }
                }
            }
            MessageBox.Show("Zdobyłeś: "+punkty.ToString()+" pkt");

        }
        private void ZadaniaKoniec_Click(object sender, RoutedEventArgs e)
        {
            Policz();
            Zakonczenie zak = new Zakonczenie();
            this.NavigationService.Navigate(zak);
        }
    }
}
